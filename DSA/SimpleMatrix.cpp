#include "stdafx.h"
#include "SimpleMatrix.h"
/**
 Copyright 2014 Maximilien Guislain 
 This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
**/

void example1()
{
	const int SIZE = 513;//(2^n)+1
	srand(2222);
	SimpleMatrix m(SIZE);
	m.setVal(SIZE-1,SIZE-1,SimpleMatrix::randVal(0,255));
	m.DSA(100);
	m.saveImage("ref.ppm");

	srand(2222);
	SimpleMatrix m3(SIZE);
	m3.setVal(SIZE-1,SIZE-1,SimpleMatrix::randVal(0,255));
	m3.DSAUnknown(100);
	m3.saveImage("Unknown.ppm");

	srand(2222);
	SimpleMatrix m2(SIZE);
	m2.setVal(SIZE-1,SIZE-1,SimpleMatrix::randVal(0,255));
	m2.CorruptedDSA(100);
	m2.saveImage("corrupt.ppm");
}

SimpleMatrix::SimpleMatrix(int size)
{
	m_matrix.resize(size*size,0.0);//full of 0, how original
	m_size= size;
}

//===================================================================================

SimpleMatrix::~SimpleMatrix(void)
{
	m_matrix.clear();
}

//===================================================================================

void SimpleMatrix::clamp(double min, double max, double & val)
{
	if(val < min)
		val = min;
	else if(val>max)
		val = max;
}

//===================================================================================

void SimpleMatrix::display()
{

	for(int j=0; j<m_size; j++)
	{
		for(int i =0; i<m_size; i++)
		{
			std::cout<< getVal(i,j);
		}
		std::cout << std::endl;
	}
}

//===================================================================================

void SimpleMatrix::DSA(double variation)
{
	int range = m_size;


	while(range >= 3)
	{
		int hrange = range/2;
		for(int i = 0; i<m_size-1; i+=range-1)
		{
			for(int j = 0; j<m_size-1; j+=range-1)
			{
				diamond(i+hrange,j+hrange,hrange,variation);

			}
		}
		for(int i = 0; i<m_size-1; i+=range-1)
		{
			for(int j = 0; j<m_size-1; j+=range-1)
			{
				square(i,j+hrange,hrange,variation);
				square(i+range-1,j+hrange,hrange,variation);
				square(i+hrange,j,hrange,variation);
				square(i+hrange,j+range-1,hrange,variation);
			}
		}


		range = hrange+1;
		variation /=2;
	}
}
//===================================================================================

void SimpleMatrix::DSAUnknown(double variation)
{
	int range = m_size;


	while(range >= 3)
	{
		int hrange = range/2;
		for(int i = 0; i<m_size-1; i+=range-1)
		{
			for(int j = 0; j<m_size-1; j+=range-1)
			{
				diamond(i+hrange,j+hrange,hrange,variation);
				square(i,j+hrange,hrange,variation);
				square(i+range-1,j+hrange,hrange,variation);
				square(i+hrange,j,hrange,variation);
				square(i+hrange,j+range-1,hrange,variation);

			}
		}

		range = hrange+1;
		variation /=2;
	}
}
//===================================================================================

void SimpleMatrix::CorruptedDSA(double variation)
{
	int range = m_size;


	while(range >= 3)
	{
		int hrange = range/2;
		for(int i = 0; i<m_size-1; i+=range-1)
		{
			for(int j = 0; j<m_size-1; j+=range-1)
			{
				diamond(i+hrange,j+hrange,hrange,variation);
			}
		}
		for(int i = 0; i<m_size-1; i+=range-1)
		{
			for(int j = 0; j<m_size-1; j+=range-1)
			{

				if(i != 0 && j+hrange != 0 /*&& i != m_size-1 && j+hrange != m_size-1*/)
				square(i,j+hrange,hrange,variation);

				if(i+range-1 != 0 && j+hrange != 0 /*&& i+range-1 != m_size-1 && j+hrange != m_size-1*/)
				square(i+range-1,j+hrange,hrange,variation);

				if(i+hrange != 0 && j != 0/* && i+hrange != m_size-1 && j != m_size-1*/)
				square(i+hrange,j,hrange,variation);

				if(i+hrange != 0 && j+range-1 != 0/* && i+hrange != m_size-1 && j+range-1 != m_size-1*/)
				square(i+hrange,j+range-1,hrange,variation);
			}
		}
		range = hrange+1;
		variation /=2;
	}
}

//===================================================================================

void SimpleMatrix::diamond(int i, int j, int hrange, double variation)
{

	double val = 0.0;
	int divider = 0;

	if(exist(i-hrange,j-hrange))
	{
		val+= getVal(i-hrange,j-hrange);
		divider++;
	}
	if(exist(i+hrange,j-hrange))
	{
		val+= getVal(i+hrange,j-hrange);
		divider++;
	}
	if(exist(i-hrange,j+hrange))
	{
		val+= getVal(i-hrange,j+hrange);
		divider++;
	}
	if(exist(i+hrange,j+hrange))
	{
		val+= getVal(i+hrange,j+hrange);
		divider++;
	}

	val/= (double)divider;//we shouldn't get a div0!, but if you see it, consider something is wrong

	val+= (double)randVal(-variation,variation);

	setVal(i,j,val);
}

//===================================================================================

void SimpleMatrix::square(int i, int j, int hrange, double variation)
{
	double val = 0.0;
	int divider = 0;

	if(exist(i,j-hrange))
	{
		val+= getVal(i,j-hrange);
		divider++;
	}
	if(exist(i,j+hrange))
	{
		val+= getVal(i,j+hrange);
		divider++;
	}
	if(exist(i+hrange,j))
	{
		val+= getVal(i+hrange,j);
		divider++;
	}
	if(exist(i-hrange,j))
	{
		val+= getVal(i-hrange,j);
		divider++;
	}

	val/= (double)divider;//we shouldn't get a div0!, but if you see it, consider something is wrong

	val+= (double)randVal(-variation,variation);

	setVal(i,j,val);
}

//===================================================================================

int SimpleMatrix::randSign()
{
	int res = rand()%2;
	if(res == 0)
		return -1;

	return 1;
}

//===================================================================================

 int SimpleMatrix::randVal(int min, int max)
{
	int val = 0;
	if(max != 0)
		val = rand()%(max-min) +min;

	return val;
}

 //===================================================================================


void SimpleMatrix::saveImage(std::string path)
{
	std::ofstream fichier(path, std::ios::out | std::ios::trunc);

	if(fichier)
	{
		
		fichier << "P2"<<std::endl;
		fichier << m_size << " " << m_size << std::endl;
		fichier << "255" << std::endl;
		for(int j = 0 ; j<m_size; j++)
		{
			for(int i = 0; i<m_size; i++)
			{
				fichier <<(int)abs(getVal(i,j)) << " ";
			}
			fichier << std::endl;
		}
		fichier.close();
	}

}

//===================================================================================

void SimpleMatrix::Voronoi(int pt)
{
	struct point
	{
		point(int i, int j,int w)
		{
			x =i; y=j; weight = w;
		}
		point(int i, int j)
		{
			x =i; y=j;
		}
		int x;
		int y;
		int weight;
	};
	std::vector<point> v;

	for(int i =0; i<pt; i++)
	{
		v.push_back(point(rand()%m_size,rand()%m_size,randVal(1,5)));
	}


	for(int j = 0 ; j<m_size; j++)
	{
		for(int i = 0; i<m_size; i++)
		{
			double mindist = distance(i,v.at(0).x,j,v.at(0).y)*v.at(0).weight;
			for(int k = 1; k< v.size(); k++)
			{
				int dist = distance(i,v.at(k).x,j,v.at(k).y)*v.at(k).weight;
				if(dist < mindist)
					mindist = dist;
				clamp(0,255,mindist);
			}
			setVal(i,j,mindist);
		}
	}
	
}

//===================================================================================

void SimpleMatrix::add(SimpleMatrix & m)
{
	//no size check
	for(int i = 0; i<m_size; i++)
	{
		for(int j = 0; j<m_size; j++)
		{
			setVal(i,j,getVal(i,j)+m.getVal(i,j));
		}
	}
}