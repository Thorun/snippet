/**
 Copyright 2014 Maximilien Guislain 
 This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
**/
#pragma once

#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <random>

void example1();

class SimpleMatrix
{
public:
	///brief ctor
	///\param int size the size of the matrix (square matrix size*size)
	SimpleMatrix(int size);

	~SimpleMatrix(void);
	/**
	* \brief this function return the value of the simple matrix located at the coordinate i,j
	* \warning this function has no check of the existence of the points, see exist(int,int)
	* \param int i the coordinate of the matrix element to return on the X axis
	* \param int j the coordinate of the matrix element to return on the Y axis
	* \return the value of the matrix element at i,j
	**/
	double getVal(int i, int j){return m_matrix.at(i+(j*m_size));}

	/**
	* \brief this function set the value of the simple matrix located at the coordinate i,j
	* \param int i the coordinate of the matrix element to return on the X axis
	* \param int j the coordinate of the matrix element to return on the Y axis
	* \param double val the value to assign at i,j
	**/
	void setVal(int i, int j, double val){m_matrix[i+(j*m_size)] = val;}

	/**
	* \brief this function return if the case i,j is inside the matrix
	* eg for a 3x3 matrix 6,0 will not be in the matrix, however 0,2 is.
	* and if you use getVal(6,0) you will end up with the result of 0,2 even if 6,0 does not exist
	* \param int i the coordinate of the matrix element to return on the X axis
	* \param int j the coordinate of the matrix element to return on the Y axis
	* \return true if the case i,j is in the matrix, false otherwise
	**/
	bool exist(int i, int j){return i>=0 && j>= 0 && i< m_size && j <m_size;}

	/**
	* \brief static function that will clam the value to [min,max]
	* \param double min the minimal value
	* \param double max the maximal value
	* \param double& val reference to the value to clamp
	**/
	static void clamp(double min, double max, double & val);

	/**
	* \brief this function will randomly return +1 or -1
	* \return int random -1 or +1
	**/
	static int randSign();

	/**
	* \brief function that will generate a value between [min,max[
	* \param min minimal value of the random interval, must be min<max, can be negative
	* \param max maximal value of the random interval, muse be max > min
	* \return int [min,max[
	**/
	static int randVal(int min, int max);

	///brief ugly display of the matrix in the console
	void display();

	///\brief will perform an term to term addition between the matrix and the matrix given into parameter
	///\param SimpleMatrix & m reference to the matrix we want to addition
	void add(SimpleMatrix & m);

	///\brief will start the diamond square algorithm
	///\param double variation, the first iteration variation in amplitude [-variation;+variation[
	void DSA(double variation);
		///\brief will start the diamond square algorithm with unknown neighbour data
	///\param double variation, the first iteration variation in amplitude [-variation;+variation[
	void DSAUnknown(double variation);

	///\this version of the diamond square will not touch the values on the top and left edges
	//\param double variation, the first iteration variation in amplitude [-variation;+variation[
	void CorruptedDSA(double variation);

	///\this function will save the matrix as a level of gray PPM image file
	//\param std::string path the filename
	void saveImage(std::string path);

	///not used yet =)
	void Voronoi(int pt);
private:

	int m_size;//!< size of an edge of the matrix

	std::vector<double> m_matrix;//!< vector of double that represent the matrix, please access through getVal and setVal

	/**
	* \brief diamond step of the diamond square algorithm
	* \param int i X coordinate of the point on which we want to make the diamond step
	* \param int j Y coordinate of the point on which we want to make the diamond step
	* \param int hrange half of the Size of the submatrix on which we are performing the step
	* \param double variation Value of the variation of the final value of the point being [-variation;+variation[
	**/
	void diamond(int i, int j, int hrange, double variation);

	/**
	* \brief square step of the diamond square algorithm
	* \param int i X coordinate of the point on which we want to make the square step
	* \param int j Y coordinate of the point on which we want to make the square step
	* \param int hrange  half of the Size of the submatrix on which we are performing the step
	* \param double variation Value of the variation of the final value of the point being [-variation;+variation[
	**/
	void square(int i, int j, int hrange, double variation);

	///brief used by voronoi, we don't care
	double distance(int x1, int x2, int y1, int y2)
	{
		return sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1)));
	}
};

